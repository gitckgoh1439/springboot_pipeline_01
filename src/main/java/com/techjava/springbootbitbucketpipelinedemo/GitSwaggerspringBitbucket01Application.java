package com.techjava.springbootbitbucketpipelinedemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitSwaggerspringBitbucket01Application {

	public static void main(String[] args) {
		SpringApplication.run(GitSwaggerspringBitbucket01Application.class, args);
	}
}
